---
title: Laravel - Learning Roadmap
date: "2020-01-13T15:40:32.169Z"
template: "post"
draft: false
slug: "laravel-learning-roadmap"
category: "Laravel"
tags:
  - "Laravel"
  - "PHP"
  - "Web Development"
description: "A laravel learning roadmap. Starting from very basics. Going towards intermediate and advanced topics. Covers online courses, blogs and books. "
socialImage: "/media/image-4.jpg"
---

This page serves as a learning roadmap for laravel.

## Courses

### Beginner

- [Master Laravel PHP for Beginners and Intermediate](https://www.udemy.com/course/laravel-beginner-fundamentals/) Also Available on coursehuner [coursehuneter link](https://coursehunter.net/course/izuchite-laravel-php-v-2019-godu-dlya-nachinayushchih-i-opytnyh)

- [Laracasts: Laravel 6 from scratch](https://laracasts.com/series/laravel-6-from-scratch)

- [RESTful API with Laravel: Build a real API with Laravel](https://coursehunter.net/course/restful-api-s-pomoshchyu-laravel-sozdayte-nastoyashchiy-api-s-laravel)

### Advanced

- [Test-Driven Laravel](https://coursehunter.net/course/adamwathan-test-driven-laravel)
- [Laravel Eloquent: Expert Level](https://coursehunter.net/course/laravel-eloquent-ekspertnyy-uroven)
- [How to Structure Laravel Project](https://coursehunter.net/course/kak-strukturirovat-proekt-laravel)
- [Scaling Laravel](https://coursehunter.net/course/masshtabirovanie-laravel)

  short series

  - [Filtering in Laravel: Blade](https://coursehunter.net/course/filtraciya-v-laravel-blade)
  -[Two Factor Authentication with Laravel](https://coursehunter.net/course/dvuhfaktornaya-autentifikaciya-s-laravel)
  - [Nuxt.js + Laravel Authentication](https://coursehunter.net/course/nuxt-js-laravel-authentication)
  - [Multistep forms with Laravel](https://coursehunter.net/course/sozdanie-mnogoshagovyh-form-s-laravel)
  - [Create a Laravel Scout Elasticsearch Driver](https://coursehunter.net/course/sozdayte-laravel-scout-elasticsearch-driver)
  - [Build snippets.codecourse.com with me](https://coursehunter.net/course/sozdayte-so-mnoy-snippets-codecourse-com)

___

### Testing

- [Confident Laravel - from no tests to confidently tested](https://coursehunter.net/course/uverennyy-laravel-ot-otsutstviya-testov-do-uverennyh-prilozheniy)

- [Laravel: PHPUnit Testing for Beginner](https://coursehunter.net/course/laravel-phpunit-testirovanie-dlya-nachinayushchih)

___

### Complete apps

- [Let's Build A Forum with Laravel and TDD](https://laracasts.com/series/lets-build-a-forum-with-laravel)
- [Build A Laravel App With TDD](https://coursehunter.net/course/postroyte-prilozhenie-laravel-s-tdd)

___

## Blogs

- [Laravel 6 Tutorial From Scratch | Step By Step](https://www.tutsmake.com/laravel-6-tutorial-from-scratch-laravel-step-by-step/?ref=hackr.io)

___

## Books

- Laravel: Up and Running
- Build and API with Laravel

## Old versions

A lot of old applications are still using Laravel 5.2 onwards so knowledge of old versions is also required

- [PHP with laravel - for beginners - covers laravel 5.2](https://www.udemy.com/course/php-with-laravel-for-beginners-become-a-master-in-laravel/?LSNPUBID=jU79Zysihs4&ranEAID=jU79Zysihs4&ranMID=39197&ranSiteID=jU79Zysihs4-Zb.RxsYisueH5D0wVxCt9Q)
